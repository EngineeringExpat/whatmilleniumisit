import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          The millennium is {Math.floor(new Date().getFullYear() / 1000)}.
        </p>
      </header>
    </div>
  );
}

export default App;
